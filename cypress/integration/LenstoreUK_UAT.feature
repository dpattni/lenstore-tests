Feature: Lenstore UK UAT


  Scenario Outline: All the sites are opening
    Given I open '<partner>' web site UAT
    Then We should see expected results 
    Examples:
        | partner     | 
        | LenstoreUK    | 
        | NextDayLenses  |

 @focus 
    Scenario: Sign in as existing user and purchase new lenses using Paypal Advanced Payments
    Given I navigate to 'https://uat.lenstore.co.uk/acuvue/daily-disposables/1-day-acuvue-moist_p13'
    When I have signed in and purchased the contact lenses using Paypal Advanced Payments
    Then the confirmation page is visible with the order number being present


@focus 
    Scenario: Sign in as a new user and purchase lenses using Paypal Advanced Payments
    Given I navigate to 'https://uat.lenstore.co.uk/acuvue/daily-disposables/1-day-acuvue-moist_p13'
    When I have signed up as a new user and purchased the contact lenses using Paypal Advanced Payments
    Then the confirmation page is visible with the order number being present


 @focus 
    Scenario: Sign in as existing user and purchase new lenses using Cybersource
    Given I navigate to 'https://uat.lenstore.co.uk/acuvue/daily-disposables/1-day-acuvue-moist_p13'
    When I have signed in and purchased the contact lenses using Cybersource
    Then the confirmation page is visible with the order number being present


@focus 
    Scenario: Sign in as a new user and purchase lenses using Cybersource
    Given I navigate to 'https://uat.lenstore.co.uk/acuvue/daily-disposables/1-day-acuvue-moist_p13'
    When I have signed up as a new user and purchased the contact lenses using Cybersource
    Then the confirmation page is visible with the order number being present


    


