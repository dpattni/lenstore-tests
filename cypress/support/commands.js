require('cypress-iframe');

Cypress.Commands.add("visitLenstore", () => {
    cy.visit('https://uat.lenstore.co.uk/');
})

Cypress.Commands.add("visitUATiManager", () => {
  cy.visit('https://uat.lenstore.co.uk/imanager');
})

Cypress.Commands.add("visitUATSites", (partner) => {
  if (partner == "LenstoreUK") {
    cy.visit('https://uat.lenstore.co.uk/');
  }
  else if (partner == "NextDayLenses") {
    cy.visit('https://www.argoseyecare.com/');
  } 
})


Cypress.Commands.add("signInToiManager", (username, password) => {
  cy.visitUATiManager();
  cy.get('[name="username"]').type(username);
  cy.get('[name="password"]').type(password);
  cy.get('[name="login"]').click();
})

 
Cypress.Commands.add("elementIsVisible", (selector) => {
    cy.get(selector).should('be.visible');
})


Cypress.Commands.add("selectPrescription", (numberofeyes, power1, bc1, dia1, power2, bc2, dia2 ) => {
  
  if (numberofeyes == "One") {
  cy.get('[data-galabel="One Eye"]').check();
  cy.get('#option1').select(power1);
  cy.get('#option2').select(bc1);
  cy.get('#option3').select(dia1);
  }

  else if (numberofeyes == "Both") {
  cy.get('[data-galabel="Both Eyes"]').check();
  cy.get('#option1').select(power1);
  cy.get('#option2').select(bc1);
  cy.get('#option3').select(dia1);
  cy.get('#option1').select(power2);
  cy.get('#option2').select(bc2);
  cy.get('#option3').select(dia2);
  }

  cy.get('#addtobasket_button').click();

})

Cypress.Commands.add("continueToCheckout", () => {
  cy.get('#payButton').click();
})


Cypress.Commands.add("signIn", (username, password) => {
  cy.get('[name="username"]').type(username);
  cy.get('[name="password"]').type(password);
  cy.get('[name="loginSubmit"]').click();
})

Cypress.Commands.add("signUpNewUser", (newemail, title, firstname, lastname, password, phone, dobday, dobmon, dobyr, country, addressL1, addressL2, town, county, postcode) => {
  cy.get('#new_email').should('be.visible').type(newemail);
  cy.get('[name="newUser"]').should('be.visible').click();

  cy.get('#title').should('be.visible').type(title);
  cy.get('#first_name').should('be.visible').type(firstname);
  cy.get('#last_name').should('be.visible').type(lastname);

  //Assert email here -  TO DO
  cy.get('#emailaddr').should('be.visible');
  cy.get('#emailconf').should('be.visible').type(newemail);
  cy.get('#passwordNew').should('be.visible').type(password);
  cy.get('#passwordConf').should('be.visible').type(password);
  cy.get('#phone').should('be.visible').type(phone);
  cy.get('#dobday').should('be.visible').select(dobday);
  cy.get('#dobmon').should('be.visible').select(dobmon);
  cy.get('#dobyr').should('be.visible').select(dobyr);

  //Enter address manually as address finder is trick on the tests
  cy.get('#manualEntry > a').should('be.visible').click();
  
  cy.get('#country').should('be.visible').select(country);
  cy.get('#add_1').should('be.visible').type(addressL1);
  cy.get('#add_2').should('be.visible').type(addressL2);
  cy.get('#town').should('be.visible').type(town);
  cy.get('#county').should('be.visible').type(county);
  cy.get('#postcode').should('be.visible').type(postcode);
  
  //select Terms and Conditions and marketing prefs
  cy.get('#tandc').should('be.visible').check()
  cy.get('#comm_soft_opt_in').should('be.visible').check()

  //Click submit
  cy.get('#ecoSubmit').should('be.visible').click();
})


Cypress.Commands.add("selectShippingMethod", (number) => {
  //Select 1st, 2nd, 3rd method in the list
  cy.get('#ship'+number).check();

  cy.get('#ecoSubmit').click();
})

//Pay using cybersource
Cypress.Commands.add("payWithCybersource", (cardtype, cardnumber, expiryMonth, expiryYear, cvn) => {
  //Review and pay screen
  cy.get('#payment_add_amex').click();
  cy.get('#payButton').click();
  

  //Select the iframe paymentWMFrame
  cy.wait(3000);
  cy.enter('#paymentWMFrame').then(getBody => {
    if (cardtype == "Visa") {
      getBody().find('#card_type_001').should('be.visible').check()
      }
    else if (cardtype == "Mastercard") {
      getBody().find('#card_type_002').should('be.visible').check()
      }
    else if (cardtype == "Amex") {
      getBody().find('#card_type_003').should('be.visible').check()
      }

      getBody().find('#card_number').should('be.visible').type(cardnumber);
      getBody().find('#card_expiry_month').should('be.visible').select(expiryMonth);
      getBody().find('#card_expiry_year').should('be.visible').select(expiryYear);
      getBody().find('#card_cvn').should('be.visible').type(cvn);
      getBody().find('[name="commit"]').should('be.visible').click();
      
  })
})

  //Pay using Paypal Advanced Payments
  Cypress.Commands.add("payWithPaypalAdvancedPayments", (cardnumber, expirydate, cvn) => {
    //Review and pay screen
    cy.get('#payment_add_visa_mastercard').click();
    //uncheck stored card flag
    cy.get('#payment_add_visa_mastercard_checkbox').should('be.visible').uncheck();
  
    cy.get('#payButton').click();
    
    //cy.get('#credit-card-number').should('be.visible').type(cardnumber);
    //cy.get('#expiration').should('be.visible').type(expirydate);
    //cy.get('#cvv').should('be.visible').type(cvn);

    cy.enter('#braintree-hosted-field-number').then(getBody => {
        getBody().find('#credit-card-number').should('be.visible').type(cardnumber);
    })

    cy.enter('#braintree-hosted-field-expirationDate').then(getBody => {
      getBody().find('#expiration').should('be.visible').type(expirydate);
    })

    cy.enter('#braintree-hosted-field-cvv').then(getBody => {
      getBody().find('#cvv').should('be.visible').type(cvn);
    })


    cy.get('#payButton').click();

    cy.wait(10000);
  
})
