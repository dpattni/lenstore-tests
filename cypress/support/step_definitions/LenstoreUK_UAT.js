import { Given, When, Then} from "cypress-cucumber-preprocessor/steps";

Given(`I open {string} web site UAT`, (partner) => {
  cy.visitUATSites(partner);
})

Given(`I navigate to {string}`, (url) => {
  cy.visit(url);
})

When('I have signed in and purchased the contact lenses using Cybersource', () => {
  
  cy.selectPrescription("One", "-4.00", "8.5", "14.2");
  cy.continueToCheckout();
  cy.signIn("Keeley32637@mailinator.com", "Tango2525");
  cy.selectShippingMethod("1");
  cy.payWithCybersource("Mastercard", "5555555555554444", "05", "2022", "123");
  
})

When('I have signed in and purchased the contact lenses using Paypal Advanced Payments', () => {
  cy.selectPrescription("One", "-4.00", "8.5", "14.2");
  cy.continueToCheckout();
  cy.signIn("Keeley32637@mailinator.com", "Tango2525");
  cy.selectShippingMethod("1");
  cy.payWithPaypalAdvancedPayments("5555555555554444", "052022", "123");
      
})
    

Then(`the confirmation page is visible with the order number being present`, () => {
  cy.wait(2000);
  cy.url().should('include', '_a=confirmed')
  cy.url().should('include', 'oid')
 //cy.log(cy.get('h1'));
 //cy.get('h1').contains('Thank you')
 //cy.get('.orderSuccess > p').contains('Thank you')


  })


  When('I have signed up as a new user and purchased the contact lenses using Paypal Advanced Payments', () => {
    cy.faker.locale = "en_GB";
    var firstname = cy.faker.name.firstName();
    var title = "Mr"
    var newemail = firstname + cy.faker.random.number() + "@mailinator.com"
    var lastname = cy.faker.name.lastName();
    var password = "Tango2525"
    var phone = cy.faker.phone.phoneNumber();
    var dobday = "31"
    var dobmon = "December"
    var dobyr = "1975"
    var country = "United Kingdom"
    var addressL1 = cy.faker.address.streetAddress();
    var addressL2 = cy.faker.address.secondaryAddress();
    var town = cy.faker.address.city();
    var county = cy.faker.address.county();
    var postcode = "SW19 8ED"

    cy.log(firstname);
  
    cy.selectPrescription("One", "-4.00", "8.5", "14.2");
    cy.continueToCheckout();
    cy.signUpNewUser(newemail, title, firstname, lastname, password, phone, dobday, dobmon, dobyr, country, addressL1, addressL2, town, county, postcode)
    cy.selectShippingMethod("1");
    cy.payWithPaypalAdvancedPayments("5555555555554444", "052022", "123");
    
      })


      When('I have signed up as a new user and purchased the contact lenses using Cybersource', () => {
        cy.faker.locale = "en_GB";
        var firstname = cy.faker.name.firstName();
        var title = "Mr"
        var newemail = firstname + cy.faker.random.number() + "@mailinator.com"
        var lastname = cy.faker.name.lastName();
        var password = "Tango2525"
        var phone = cy.faker.phone.phoneNumber();
        var dobday = "31"
        var dobmon = "December"
        var dobyr = "1975"
        var country = "United Kingdom"
        var addressL1 = cy.faker.address.streetAddress();
        var addressL2 = cy.faker.address.secondaryAddress();
        var town = cy.faker.address.city();
        var county = cy.faker.address.county();
        var postcode = "SW19 8ED"
    
        cy.log(firstname);
      
        cy.selectPrescription("One", "-4.00", "8.5", "14.2");
        cy.continueToCheckout();
        cy.signUpNewUser(newemail, title, firstname, lastname, password, phone, dobday, dobmon, dobyr, country, addressL1, addressL2, town, county, postcode)
        cy.selectShippingMethod("1");
        cy.payWithCybersource("Mastercard", "5555555555554444", "05", "2022", "123");
        
          })
    


