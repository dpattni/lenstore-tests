// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

cy.faker = require('faker');

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

beforeEach(() => {
    // now this runs prior to every test
    // across all files no matter what
    cy.signInToiManager("Dillan.Pattni", "Icecream100!");
  })

// Alternatively you can use CommonJS syntax:
// require('./commands')
